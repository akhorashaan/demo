<?php

require 'vendor/autoload.php';

use App\Connection\Db;
use App\Repository\InnRepository;
use App\Services\DateComparerService;
use App\Services\GetInnStatusService;
use App\Services\ResponseFormatterService;

header('Content-Type: application/json');

try {
    $requestParams = $_GET;

    if (!array_key_exists('inn', $requestParams)) {
        throw new InvalidArgumentException('Inn not found');
    }

    $inn = $requestParams['inn'];

    $dbName = getenv('POSTGRES_DB');
    $dbUser = getenv('POSTGRES_USER');
    $dbPass = getenv('POSTGRES_PASSWORD');

    $connection = new Db($dbName, $dbUser, $dbPass);
    $pdo = $connection->getConnection();
    $repository = new InnRepository($pdo);
    $data = $repository->findExisted($inn);

    if ($data && !DateComparerService::isRecordDateIsOldEnough($data['check_date'])) {
        $result = ResponseFormatterService::prepareSuccess($data['status']);
    } else {
        $innStatusService = new GetInnStatusService();
        $result = $innStatusService->getStatusForInn($inn);
    }

    $response = json_encode($result, JSON_THROW_ON_ERROR);
    echo $response;

    if ($result['status'] !== 'ok') {
        die();
    }

    if ($data) {
        $repository->updateData($result['result'], $data);
    } else {
        $repository->createData($result['result'], $inn);
    }

    die();
} catch (Throwable $e) {
    echo json_encode(ResponseFormatterService::prepareError('Неизвестная ошибка'), JSON_THROW_ON_ERROR);
    die();
}
