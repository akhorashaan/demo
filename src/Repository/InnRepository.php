<?php

declare(strict_types=1);

namespace App\Repository;

use DateTimeImmutable;
use PDO;

final class InnRepository
{
    public function __construct(private PDO $pdo)
    {
    }

    public function findExisted(string $inn)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM inn WHERE inn_number=:innNumber");
        $stmt->execute([':innNumber' => $inn]);

        return $stmt->fetch();
    }

    public function createData(bool $status, string $inn): void
    {
        $date = new DateTimeImmutable('now');
        $sql = "INSERT INTO inn (inn, is_self_employed, check_date) VALUES (:inn, :isSelfEmployed, :checkDate)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([
            ':inn' => $inn,
            ':checkDate' => $date->format('Y-m-d H:i:s'),
            ':isSelfEmployed' => $status,
        ]);
    }

    public function updateData(bool $status, array $data = null): void
    {
        $date = new DateTimeImmutable('now');
        $sql = "UPDATE inn SET is_self_employed=:isSelfEmployed, check_date=:checkDate WHERE id=:id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([
            ':id' => $data['id'],
            ':checkDate' => $date->format('Y-m-d H:i:s'),
            ':isSelfEmployed' => $status,
        ]);
    }
}
