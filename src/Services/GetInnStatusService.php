<?php

declare(strict_types=1);

namespace App\Services;

final class GetInnStatusService
{
    private const SERVICE_URL = 'https://statusnpd.nalog.ru:443/api/v1/tracker/taxpayer_status';

    private function makeRequest(string $inn, $curl): bool|string
    {
        $data = [
            'inn' => $inn,
            'requestDate' => date("Y-m-d"),
        ];

        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));

        return curl_exec($curl);
    }

    public function getStatusForInn(string $inn): array
    {
        $url = self::SERVICE_URL;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = $this->makeRequest($inn, $curl);

        if (!curl_errno($curl)) {
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            $data = json_decode($result, true, 512, JSON_THROW_ON_ERROR);

            if ($httpCode > 399) {
                return ResponseFormatterService::prepareError($data['message']);
            }

            return ResponseFormatterService::prepareSuccess($data['status']);
        }

        curl_close($curl);

        return ResponseFormatterService::prepareError('Неизвестная ошибка');
    }
}
