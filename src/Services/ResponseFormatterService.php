<?php

declare(strict_types=1);

namespace App\Services;

final class ResponseFormatterService
{
    public static function prepareError(string $error): array
    {
        return [
            'status' => 'error',
            'message' => $error,
        ];
    }

    public static function prepareSuccess(bool $status): array
    {
        return [
            'status' => 'ok',
            'result' => $status,
        ];
    }
}
