<?php

declare(strict_types=1);

namespace App\Services;

use DateInterval;
use DateTime;

final class DateComparerService
{
    private const PERIOD = 'P1D';

    public static function isRecordDateIsOldEnough(string $recordDate): bool
    {
        $date = new Datetime('now');
        $interval = new DateInterval(self::PERIOD);
        $date->sub($interval);
        $checkDate = DateTime::createFromFormat('Y-m-d H:i:s', $recordDate);

        return $checkDate < $date;
    }
}
