<?php

declare(strict_types=1);

namespace App\Connection;

use PDO;
use PDOException;
use RuntimeException;

final class Db
{
    public function __construct(
        private string $dbName,
        private string $dbUser,
        private string $dbPass
    ) {
    }

    public function getConnection(): PDO
    {
        $dsn = "pgsql:host=postgres-inn;port=5432;dbname=$this->dbName;";


        return new PDO($dsn, $this->dbUser, $this->dbPass, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ]);
    }
}
