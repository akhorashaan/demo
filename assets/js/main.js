/*jshint esversion: 6 */
'use strict';

const form = document.getElementById('form');
const input = document.getElementById('innInput');
const submitButton = document.getElementById('submitForm');
const errorString = document.getElementById('errorText');

const isInputDigitsOnly = (string) => {
    const regex = new RegExp(/^[0-9]+$/);

    return regex.test(string);
};

const isSuitableInnLength = (string) => {
    return string.length === 10 || string.length === 12;
};

const getControlNumber = (controlSum) => {
    return controlSum % 11 % 10;
};

const isInn = (string) => {
    if (string.length === 10) {
        const controlSum = parseInt(string[0], 10) * 2
            + parseInt(string[1], 10) * 4
            + parseInt(string[2], 10) * 10
            + parseInt(string[3], 10) * 3
            + parseInt(string[4], 10) * 5
            + parseInt(string[5], 10) * 9
            + parseInt(string[6], 10) * 4
            + parseInt(string[7], 10) * 6
            + parseInt(string[8], 10) * 8;
        const controlNumber = getControlNumber(controlSum);

        return controlNumber === parseInt(string[9], 10);
    }

    const controlSum = parseInt(string[0], 10) * 7
        + parseInt(string[1], 10) * 2
        + parseInt(string[2], 10) * 4
        + parseInt(string[3], 10) * 10
        + parseInt(string[4], 10) * 3
        + parseInt(string[5], 10) * 5
        + parseInt(string[6], 10) * 9
        + parseInt(string[7], 10) * 4
        + parseInt(string[8], 10) * 6
        + parseInt(string[9], 10) * 8;
    const controlNumber = getControlNumber(controlSum);
    const controlSumAdditional = parseInt(string[0], 10) * 3
        + parseInt(string[1], 10) * 7
        + parseInt(string[2], 10) * 2
        + parseInt(string[3], 10) * 4
        + parseInt(string[4], 10) * 10
        + parseInt(string[5], 10) * 3
        + parseInt(string[6], 10) * 5
        + parseInt(string[7], 10) * 9
        + parseInt(string[8], 10) * 4
        + parseInt(string[9], 10) * 6
        + parseInt(string[10], 10) * 8;
    const controlNumberAdditional = getControlNumber(controlSumAdditional);

    return controlNumber === parseInt(string[10], 10) && controlNumberAdditional === parseInt(string[11], 10);
};

const showInvalidInput = (errorField) => {
    errorField.style.display = 'block';
};

const hideInvalidInput = (errorField) => {
    errorField.style.display = 'none';
};

input.addEventListener('input', () => {
    hideInvalidInput(errorString);
});

submitButton.addEventListener('click', async (e) => {
    e.preventDefault();

    const value = input.value.trim();

    if (!isInputDigitsOnly(value) || !isSuitableInnLength(value) || !isInn(value)) {
        showInvalidInput(errorString);

        return;
    }

    submitButton.disabled = true;
    const action = form.action;
    const response = await fetch(`${action}?inn=${value}`);

    if (response.ok) {
        let json = await response.json();

        if (json.status === 'error') {
            alert(`Ошибка: ${json.message}`);
            submitButton.disabled = false;

            return;
        }

        if (json.result) {
            alert('Пользователь является самозанятым');
        } else {
            alert('Пользователь не является самозанятым');
        }
    } else {
        alert('Неизвестная ошибка');
    }

    submitButton.disabled = false;
});
