<?php

require 'vendor/autoload.php';

use App\Connection\Db;

$dbName = getenv('POSTGRES_DB');
$dbUser = getenv('POSTGRES_USER');
$dbPass = getenv('POSTGRES_PASSWORD');

try {
    $connection = new Db($dbName, $dbUser, $dbPass);
    $pdo = $connection->getConnection();

    $tableSql = "CREATE TABLE IF NOT EXISTS inn (
        id SERIAL PRIMARY KEY,
        inn_number VARCHAR(13) NOT NULL, 
        is_self_employed BOOLEAN NOT NULL,
        check_date TIMESTAMP NOT NULL
    );";
    $pdo->exec($tableSql);
    $indexSql = "CREATE UNIQUE INDEX idx_inn_number ON inn(inn_number);";
    $pdo->exec($indexSql);
} catch (Throwable $e) {
    die($e->getMessage());
} finally {
    $connection = null;
}
